<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\RequestDemoForm;
use Carbon\Carbon;
use App\Mail\RequestDemo;
use App\Mail\RequestDemoAdmin;
use Notification;
use Illuminate\Support\Facades\Mail;


/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $requests = RequestDemoForm::where('user_id', auth()->user()->id)->paginate(10);

        return view('frontend.user.dashboard', compact('requests'));
    }

    public function showRequestDemo()
    {
        return view('frontend.user.request-demo');
    }

    public function showGuidelines()
    {
        return view('frontend.user.guidelines');
    }

    public function submitDemoForm()
    {
        $requestDemo = new RequestDemoForm();
        $requestDemo->user_id = auth()->user()->id;
        $requestDemo->reference_no = Carbon::now()->timestamp;
        $requestDemo->status = 'pending'; // Initial Status
        $requestDemo->request_demo_details = json_encode(request()->all());
        $requestDemo->save();
        $mail_admin = 'centeno.marisol1219+8@gmail.com';


        if($requestDemo){
            Mail::to(auth()->user()->email)->send(new RequestDemo($requestDemo));
            Mail::to($mail_admin)->send(new RequestDemoAdmin($requestDemo));
        }


        return redirect()->route('frontend.user.dashboard')->withFlashSuccess('Successfully requested a demo with reference# ' . $requestDemo->reference_no);
    }

    public function viewDemoDetails($id)
    {
        $demoDetails = RequestDemoForm::find($id);
        $activityCategory = json_decode($demoDetails->request_demo_details)->activity_category;
        return view('frontend.user.request-demo-view', compact('demoDetails', 'activityCategory'));   
    }
}
