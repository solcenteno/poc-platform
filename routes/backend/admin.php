<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\RequestDemoController;
use Tabuna\Breadcrumbs\Trail;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('admin.dashboard'));
    });

// Requests Demo List
Route::get('dashboard/requests-demo-list', [RequestDemoController::class, 'index'])
    ->name('dashboard.requests-demo-list')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('admin.dashboard'));
        $trail->push('Requests Demo List', '');
    });

// Approve Demo
Route::get('dashboard/request-demo/approve/{id}', [RequestDemoController::class, 'approveDemo'])
    ->name('dashboard.request-demo-approve');

// Reject Demo
Route::get('dashboard/request-demo/reject/{id}', [RequestDemoController::class, 'rejectDemo'])
->name('dashboard.request-demo-reject');
