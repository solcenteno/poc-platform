@extends('backend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            Requests Demo List
        </x-slot>

        <x-slot name="body">
            <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                      <th scope="col">Reference Number</th>
                      <th scope="col">Status</th>
                      <th scope="col">Submitted At</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($requestsDemo as $request)
                    <tr>
                        <th scope="row">{{ $request->reference_no}}</th>
                        <td>{{ $request->status}}</td>
                        <td>{{ $request->created_at}}</td>
                        <td>
                            <button class="btn btn-primary btn-sm">VIEW</button>
                            <a href="{{ route('admin.dashboard.request-demo-approve', $request->id) }}"><button class="btn btn-success btn-sm">APPROVE</button></a>
                            <a href="{{ route('admin.dashboard.request-demo-reject', $request->id) }}"><button class="btn btn-danger btn-sm">REJECT</button></a>
                        </td>
                    </tr>
                  @endforeach
                  </tbody>
              </table>
                {{ $requestsDemo->links() }}

        </x-slot>
    </x-backend.card>
@endsection

