<div>
    Hi Administrator,
    <br>
    <br>
    Someone is requesting a demo for your review and approval.
    <br>
    <br>
    Reference Number: {{ $details->reference_no }}
    <br>
    <br>
    Regards,
    <br>
    VST ECS PHIL INC.
</div>