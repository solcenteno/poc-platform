@extends('frontend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <x-frontend.card>
                    
                    <x-slot name="header">
                        Request a Demo
                    </x-slot>

                    <x-slot name="body">
                        <form action="{{ route(
                            'frontend.user.request-demo') }}" method="POST">
                        {{ csrf_field() }}
                            <div class="p-3 mb-2 bg-secondary text-white">
                        
                                <label for="test_label">Activity Category</label>
                                <div class="row">
                                <legend class="col-form-label col-sm-2 pt-0"></legend>
                                <div class="col-sm-10">
                                    <div class="form-check">
                                    <input class="form-check-input" type="radio" name="activity_category" id="gridRadios1" value="1" {{ json_decode($demoDetails->request_demo_details)->activity_category == 1 ? 'checked' : ''}}>
                                    <label class="form-check-label" for="gridRadios1">
                                    Customer Experience Workshop
                                    </label>
                                    </div>
                                    
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios2" value="2" {{ json_decode($demoDetails->request_demo_details)->activity_category == 2 ? 'checked' : ''}}>
                                <label class="form-check-label" for="gridRadios2">
                                    Business Partner Develeopment/Workshop
                                </label>
                                </div>

                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios3" value="3" {{ json_decode($demoDetails->request_demo_details)->activity_category == 3 ? 'checked' : ''}}>
                                <label class="form-check-label" for="gridRadios3">
                                    Virtual POC/POT
                                </label>
                                </div>

                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios4" value="4"{{ json_decode($demoDetails->request_demo_details)->activity_category == 4 ? 'checked' : ''}}>
                                <label class="form-check-label" for="gridRadios4">
                                    iSupport & Education Services
                                </label>
                                </div>

                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="activity_category" id="gridRadios5" value="5"{{ json_decode($demoDetails->request_demo_details)->activity_category == 5 ? 'checked' : ''}}>
                                <label class="form-check-label" for="gridRadios5">
                                    Internal Development/TCT
                                </label>
                                </div>

                                </div>
                                </div><br>
                            
                        
                                <div class="activity_cat_1"> 

                            <div class="p-3 mb-2 bg-info text-white">
                                Project Details 
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Project Name</label>
                                <input type="text" class="form-control" name="projectName_cat1" placeholder="Input Project Name" id="project_name_cat1" value="{{json_decode($demoDetails->request_demo_details)->projectName_cat1}}"readonly>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Project Description</label>
                                <input type="text" class="form-control" name="projectDecription_cat1" placeholder="Input Project Details and Objective" id="project_description_cat1" value="{{json_decode($demoDetails->request_demo_details)->projectDecription_cat1}}"readonly>
                            </div>

                            <div class="form-group row">
                                <label for="example-date-input" class="col-2 col-form-label">Date Start</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="2020-07-01" name="dateStart_cat1" id="date_start_cat1" value="{{json_decode($demoDetails->request_demo_details)->dateStart_cat1}}"readonly>
                            </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-date-input" class="col-2 col-form-label">Date End</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="2020-07-01" name="dateEnd_cat1" id="date_end_cat1" value="{{json_decode($demoDetails->request_demo_details)->dateEnd_cat1}}"readonly>
                            </div>
                            </div>

                            <div class="btn-group">
                                <label for="formGroupExampleInput">Product</label>

                                <div class="form-group col-md-12">
                                <select id="product_cat1" class="form-control">
                                
                                    <option selected></option>
                                    <option {{json_decode($demoDetails->request_demo_details)->product_cat1 == 'Oracle' ? 'selected' : ''}}>Oracle</option>
                                    <option {{json_decode($demoDetails->request_demo_details)->product_cat1 == 'Cisco' ? 'selected' : ''}}>Cisco</option>
                                    <option {{json_decode($demoDetails->request_demo_details)->product_cat1 == 'Huawei' ? 'selected' : ''}}>Huawei</option>
                                    <option {{json_decode($demoDetails->request_demo_details)->product_cat1 == 'VMware' ? 'selected' : ''}}>VMware</option>
                                    <option {{json_decode($demoDetails->request_demo_details)->product_cat1 == 'SAP' ? 'selected' : ''}}>SAP</option>
                                    <option {{json_decode($demoDetails->request_demo_details)->product_cat1 == 'Pivotal' ? 'selected' : ''}}>Pivotal</option>
                                </select>
                                </div>
                            </div>
                                                            
                            <div class="form-group">
                                <label for="formGroupExampleInput">Estimated Size of Oppurtunity(PHP)</label>
                                <input type="text" class="form-control" name="OppurtunitySize_cat1" placeholder="Input estimated size of oppurtunity" id="oppurtunity_cat1" value="{{json_decode($demoDetails->request_demo_details)->OppurtunitySize_cat1}}"readonly>
                            </div>
                            </div>

                            <div class="activity_cat_2">

                            <div class="p-3 mb-2 bg-info text-white">
                                Reseller Details
                            </div>

                            <div class="p-3 mb-2 bg-secondary text-white">

                                <div class="btn-group">
                                    <label for="formGroupExampleInput">Company</label>
                                
                                    <div class="form-group col-md-12">
                                    <select class="form-control" id="company_cat2" name="company_cat2" value="{{json_decode($demoDetails->request_demo_details)->company_cat2}}"readonly>
                                        <option selected>Select your company</option>
                                        <option>Red Rock Security</option>
                                        <option>Shellsoft Technology Corporation</option>
                                        <option>Nexus Technologies, Incorporated</option>
                                        <option>Micro-D International Incorporated</option>
                                        <option>Questronix Corporation</option>
                                        <option>OneDepot</option>
                                    </select>
                                    </div>

                                </div>

                            </div>
                                                            
                            <div class="form-group">
                                <label for="formGroupExampleInput">Contact Person</label>
                                <input type="text" class="form-control" name="partnerContactPerson_cat2" placeholder="Input Contact Person" id="contact_person_cat2" value="{{json_decode($demoDetails->request_demo_details)->partnerContactPerson_cat2}}"readonly>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Email Address</label>
                                <input type="text" class="form-control" name="partnerEmailAddress_cat2" placeholder="Input Email Address" id="email_cat2" value="{{json_decode($demoDetails->request_demo_details)->partnerEmailAddress_cat2}}"readonly>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Designation</label>
                                <input type="text" class="form-control" name="partnerDesignation_cat2" placeholder="Input Designation" id="designation_cat2" value="{{json_decode($demoDetails->request_demo_details)->partnerDesignation_cat2}}"readonly>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Mobile/Phone</label>
                                <input type="text" class="form-control" name="partnerContactDetails_cat2" placeholder="Input Contact Number" id="mobile_cat2" value="{{json_decode($demoDetails->request_demo_details)->partnerContactDetails_cat2}}"readonly>
                            </div>

                            </div>

                            <div class="activity_cat_3">

                            <div class="p-3 mb-2 bg-info text-white">
                                Customer Details
                            </div>

                            <div class="btn-group">
                                <label for="formGroupExampleInput">Company</label>

                                <select name="company_cat3" id="company_cat3" class="form-control">
                                    <option selected>Select your company</option>
                                    <option>Red Rock Security</option>
                                    <option>Shellsoft Technology Corporation</option>
                                    <option>Nexus Technologies, Incorporated</option>
                                    <option>Micro-D International Incorporated</option>
                                    <option>Questronix Corporation</option>
                                    <option>OneDepot</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="formGroupExampleInput">Contact Person</label>
                                <input type="text" class="form-control" name="customerContactPerson_cat3" placeholder="Input Contact Person">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Email Address</label>
                                <input type="text" class="form-control" name="customerEmailAddress_cat3" placeholder="Input Email Address">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Designation</label>
                                <input type="text" class="form-control" name="customerDesignation_cat3" placeholder="Input Designation">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Mobile/Phone</label>
                                <input type="text" class="form-control" name="customerContactDetails_cat3" placeholder="Input Contact Number">
                            </div>
                            </div>

                                            
                            <div class="activity_cat_4">
                            <div class="p-3 mb-2 bg-info text-white">
                            Compute Requirements
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Compute Name</label>
                                <input type="text" class="form-control" name="compute1Name_cat4" placeholder="Input Compute Name">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">vCPU(1-8)</label>
                                <input type="text" class="form-control" name="compute1Vcpu_cat4" placeholder="Input vCPU">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Memory (GB)</label>
                                <input type="text" class="form-control" name="compute1Memory_cat4" placeholder="Input Memory">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Operating System</label>
                                <input type="text" class="form-control" name="compute1OS_cat4" placeholder="Input OS">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">No. of NIC</label>
                                <input type="text" class="form-control" name="compute1Nic_cat4" placeholder="Input # of NIC">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">AD Required?</label>
                                <input type="text" class="form-control" name="compute1Requirements_cat4" placeholder="Input Additional Requirements">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Others (Pls specify)</label>
                                <input type="text" class="form-control" name="compute1Others_cat4" placeholder="Input others">
                            </div>
                            </div>

                            <div class="activity_cat_5"> 
                            <div class="p-3 mb-2 bg-info text-white">
                            Customer Experience Workshop Details 
                            </div>

                            <div class="form-group">
                            <label for="formGroupExampleInput">Event Name</label>
                            <input type="text" class="form-control" name="eventName_cat5" placeholder="Input Event Name">
                            </div>

                            <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">Date Start</label>
                            <div class="col-10">
                            <input class="form-control" type="date" value="2020-09-01" name="dateStart_cat5" id="example-date-input">
                            </div>
                            </div>

                            <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">Date End</label>
                            <div class="col-10">
                            <input class="form-control" type="date" value="2020-09-01" name="dateEnd_cat5" id="example-date-input">
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="formGroupExampleInput">Duration</label>
                            <input type="text" class="form-control" name="duration_cat5" placeholder="Input Duration">
                            </div>

                            <div class="btn-group">
                            <label for="formGroupExampleInput">Product</label>

                            <div class="form-group col-md-12">
                                <select name="product_cat5" id="product_cat5" class="form-control" placeholder="Select Product">
                                    <option selected></option>
                                    <option>Oracle</option>
                                    <option>Cisco</option>
                                    <option>Huawei</option>
                                    <option>VMware</option>
                                    <option>SAP</option>
                                    <option>Pivotal</option>
                                </select>
                            </div>
                            </div><br>

                            <div class="btn-group">
                            <label for="formGroupExampleInput">Participants Profile</label>

                            <div class="form-group col-md-12">
                                <select name="ParticipantProfile_cat5" id="inputState" class="form-control" placeholder="Select Participants">
                                    <option selected></option>
                                    <option>Technical</option>
                                    <option>Sales</option>
                                    <option>Executives</option>
                                    <option>Mixed (Technical | Sales)</option>
                                    <option>Mixed (Sales | Executive)</option>
                                    <option>Mixed (Technical | Executive)</option>
                                </select>
                            </div>
                            </div>  

                            <div class="form-group">
                            <label for="formGroupExampleInput">Target No. Attendees</label>
                            <input type="text" class="form-control" name="attendees_cat5" placeholder="Input Number of attendees">
                            </div>

                            </div>




                                                            
                            <div class="activity_cat_6"> 
                            <div class="p-3 mb-2 bg-info text-white">
                            Business Partner Development/Workshop 
                            </div>

                            <div class="form-group">
                            <label for="formGroupExampleInput">Event Name</label>
                            <input type="text" class="form-control" name="eventName_cat6" placeholder="Input Event Name">
                            </div>

                            <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">Date Start</label>
                                <div class="col-10">
                                    <input class="form-control" type="date" value="2020-09-01" name="dateStart_cat6" id="example-date-input">
                                </div>
                            </div>

                            <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">Date End</label>
                                <div class="col-10">
                                    <input class="form-control" type="date" value="2020-09-01" name="dateEnd_cat6" id="example-date-input">
                                </div>
                            </div>

                            <div class="form-group">
                            <label for="formGroupExampleInput">Duration</label>
                            <input type="text" class="form-control" name="duration_cat6" placeholder="Input Duration">
                            </div>

                            <div class="btn-group">
                            <label for="formGroupExampleInput">Product</label><br>

                            <div class="form-group col-md-12">
                            <select name="product_cat6" id="product_cat6" class="form-control">
                                <option selected></option>
                                <option>Oracle</option>
                                <option>Cisco</option>
                                <option>Huawei</option>
                                <option>VMware</option>
                                <option>SAP</option>
                                <option>Pivotal</option>
                            </select>
                            </div>
                            </div><br>

                            <div class="btn-group">
                            <label for="formGroupExampleInput">Participants Profile</label><br>

                            <div class="form-group col-md-12">
                            <select name="participants_cat6" id="participants_cat6" class="form-control">
                                <option selected></option>
                                <option>Technical</option>
                                <option>Sales</option>
                                <option>Executives</option>
                                <option>Mixed</option>
                            </select></div>
                            </div>

                            <div class="form-group">
                            <label for="formGroupExampleInput">Target No. Attendees</label>
                            <input type="text" class="form-control" name="attendees_cat6" placeholder="Input Target No of Attendees">
                            </div>


                            <!-- <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <input type="checkbox" name="sent_email_response" aria-label="Checkbox for following text input">
                                Send me an email receipt of my responses                               
                            </div>
                            </div> -->
                            </div>
                                                        
                                                                                            
                        <div class="activity_cat_7"> 
                            <div class="p-3 mb-2 bg-info text-white">
                                VST-ECS Contacts (Optional)
                            </div>
                                                    
                            <div class="form-group">
                                <label for="formGroupExampleInput">VST-ECS Engineer</label>
                                <input type="text" class="form-control" name="Engineer_cat7" placeholder="Input Engineer Name"value="{{json_decode($demoDetails->request_demo_details)->Engineer_cat7}}" readonly>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">VST-ECS Product Manager</label>
                                <input type="text" class="form-control" name="productManager_cat7" placeholder="Input Product Manager Name">
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Others</label>
                                <input type="text" class="form-control" name="others_cat7" placeholder="Input Others">
                            </div>
                        </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Target No. Attendees</label>
                                <input type="text" class="form-control" name="number_of_attendees" placeholder="Input Target No of Attendees">
                            </div>
                                                    
                        </div>

                        <div class="card-header-actions">
                            <a href="http://localhost:8000/dashboard" class="card-header-action">Back</a>
                        </div>   
                        
                    </form>
                    </x-slot>
                </x-frontend.card>
            </div><!--col-md-10-->
        </div><!--row-->
    </div><!--container-->
@endsection

@push('after-scripts')
    <script>
        $('.activity_cat_1').hide();
        $('.activity_cat_2').hide();
        $('.activity_cat_3').hide();
        $('.activity_cat_4').hide();
        $('.activity_cat_5').hide();
        $('.activity_cat_6').hide();
        $('.activity_cat_7').hide();
        
        $test = {!! $activityCategory !!};
        console.log($test);
        var activityCategory = $("input[name='activity_category']:checked").val();
            console.log(activityCategory);
            // Condition what fields be shown in form
            

            // Customer experience Workshop
            if (activityCategory == 1){ 

                $("#elementId").attr('required', 'true');
                
                $('.activity_cat_1').hide();
                $('.activity_cat_2').hide();
                $('.activity_cat_3').hide();
                $('.activity_cat_4').hide();
                $('.activity_cat_5').show();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').show();
            }

             // Business Partner Development / Workshop
             if (activityCategory == 2){
                $('.activity_cat_1').hide();
                $('.activity_cat_2').hide();
                $('.activity_cat_3').hide();
                $('.activity_cat_4').hide();
                $('.activity_cat_5').hide();
                $('.activity_cat_6').show();
                $('.activity_cat_7').show();
               
            }


            // Virtual POC / POT
            if (activityCategory == 3){
                $("#project_name_cat1").attr('required', 'true');
                $("#project_description_cat1").attr('required', 'true');
                $("#date_start_cat1").attr('required', 'true');
                $("#date_end_cat1").attr('required', 'true');
                $("#oppurtunity_cat1").attr('required', 'true');

                $('.activity_cat_1').show();
                $('.activity_cat_2').show();
                $('.activity_cat_3').show();
                $('.activity_cat_4').show();
                $('.activity_cat_5').hide();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').show();
            
            }

            // iSupport & Education Services
            if (activityCategory == 4){
                $('.activity_cat_1').show();
                $('.activity_cat_2').show();
                $('.activity_cat_3').show();
                $('.activity_cat_4').show();
                $('.activity_cat_5').hide();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').show();
            
            }

            // Internal Development / TCT
            if (activityCategory == 5){
                $('.activity_cat_1').show();
                $('.activity_cat_2').hide();
                $('.activity_cat_3').hide();
                $('.activity_cat_4').show();
                $('.activity_cat_5').hide();
                $('.activity_cat_6').hide();
                $('.activity_cat_7').hide();
            
            }
    </script>
@endpush