@extends('frontend.layouts.app')

@section('title', __('Guidelines'))

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <x-frontend.card>
                    <x-slot name="header">
                        @lang('PROVISIONING GUIDELINES FOR PARTNERS')
                    </x-slot>

                    <x-slot name="body">
                        <p>1.) Upon submission of the MSI POC form from the website, MSI POC DC admins will provide the needed guest VM as early as the next business day. Provisioning time 
                                may vary depending on the brequirements and number of guests to be deployed. *MSI POC form still have bugs 
                        <br>2.) Provisioning of guest VMs will be up to Operating System installation at the minimum. Additional installation of software/applications depends on the scope of 
                        request. 
                        <br>3.) After provisioning of guest VMs, MSI POC DC admins will send an email containing all information of the created guest VMs: IP address, login credentials, remote 
                        access information etc. 
                        <br>4.) For remote access, VPN login credentials will be provided once approved by the VST ECS network team.'; 
                        <br>5.) Changes on the resources (cores, memory etc.) of the guest VMs, need to fill out a Change Request Form. After submitting the form, allow MSI POC DC admins at
                        least a day to make necessary changes on the guest VMs. *Change Request form not yet created. 
                        <br>6.) Access of guest VMs is from Monday 10am to Friday 5pm only. Since MSI shuts down all machines on weekends as a company protocol. 
                        <br>7.) Access on weekends is for approval and must send an email to the MSI POC DC admins about the details of the activity to be done. 
                        <br>8.) For connectivity issues or other related problems and inquiries kindly notify MSI POC DC admins thru email. Expect at least 1-2 hours response time. 
                        <br>9.) Submitting of form above means accepting the above provisioning guidelines and rules below.

                        
                    </x-slot>
                </x-frontend.card>
            </div><!--col-md-10-->
        </div><!--row-->
    </div><!--container-->

    <div class="container py-4">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <x-frontend.card>
                    <x-slot name="header">
                        @lang('POC DATACENTER RULES')
                    </x-slot>

                    <x-slot name="body">
                        <p>1.) For onsite visit with partners, you are required to fill up the login/logout sheet before entering POC DC. 
                        <br>2.) The engineer cannot leave the partner alone inside the POC data center. 
                        <br>3.) No foods and drinks allowed inside the POC data center. 
                        <br>4.) Rack cabinets will be locked, if you need access to the machines physically or will be racking machines, kindly request the key to the MSI POC DC admins. 
                        <br>5.) Do not leave the white light open and the rack cabinets open if there is no one in the room. 
                        <br>6.) vCenter and the VMs can be accessed through the Support Network, Cloud Rooms and thru MSI Support POC Wifi. 

                        
                    </x-slot>
                </x-frontend.card>
            </div><!--col-md-10-->
        </div><!--row-->
    </div><!--container-->
@endsection
                 
                        