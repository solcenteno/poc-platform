# Inventory Demo Unit

## Install Dependencies

### Composer
- **[Download Composer](https://getcomposer.org/)**

### XAMPP
- **[Download XAMPP](https://www.apachefriends.org/index.html)**

### Node
- **[Download Node](https://nodejs.org/en/)**
- after installing need to restart 

### Git Bash
- **[Download Git Bash](https://git-scm.com/downloads)**

### Run Project locally
- git clone https://gitlab.com/emanmjr5/inventory-demo.git
- cd to "project_directory"
- run "composer install"
- run "npm install"
- Create .env and Copy .env.example
- run "php artisan key:generate"
- run "php artisan serve" to run in localhost
