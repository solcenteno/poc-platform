<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestDemoFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_demo_forms', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('reference_no');
            $table->enum('status', ['pending', 'approved', 'in-progress', 'rejected']);
            $table->longText('request_demo_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_demo_forms');
    }
}
